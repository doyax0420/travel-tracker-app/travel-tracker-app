import React, { useEffect, useState, useRef } from 'react';
import Head from 'next/head';
import { Table, Alert, Row, Col } from 'react-bootstrap';
import moment from 'moment';
import mapboxgl from 'mapbox-gl';
mapboxgl.accessToken = process.env.NEXT_PUBLIC_REACT_APP_MAPBOX_KEY;

export default function History() {

	// State for storing all the records from the database.
	const [records, setRecords] = useState([])
	const [longitude, setLongitude] = useState(0);
	const [latitude, setLatitude] = useState(0);

	const mapContainerRef = useRef(null);

	function setCoordinates(long, lat){
        setLongitude(long)
        setLatitude(lat)
    }

	// Get the user's record when the component mounts
    useEffect(() => {

        fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/users/details`, {
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res => res.json())
        .then(data => {

            if(data._id){ //JWT validated
                setRecords(data.travels)
            }else{ //JWT is invalid or non-existent
                setRecords([])
            }            

        })

    }, [])

	// Initialize map when this component mounts, rerender when changes are made to coordinates in component state
    useEffect(() => {

        // Instantiate a new Mapbox Map object
        const map = new mapboxgl.Map({
            // Set the container for the map as the current component
            container: mapContainerRef.current,
            // See style options here: https://docs.mapbox.com/api/maps/#styles
            style: 'mapbox://styles/mapbox/streets-v11',
            center: [longitude, latitude],
            // Zoomed all the way out to show the entire world
            zoom: 12
        })

        // Create a marker centered on the designated longitude and latitude
        const marker = new mapboxgl.Marker()
        .setLngLat([longitude, latitude])
        .addTo(map)
      
        // Add navigation control (the +/- zoom buttons)
        map.addControl(new mapboxgl.NavigationControl(), 'bottom-right')
    
        // Clean up and release all resources associated with this map when component unmounts
        // Not doing so will result in an ever-increasing consumption of RAM, eventually crashing your dev machine
        return () => map.remove()

    }, [longitude, latitude])

	return(
		<React.Fragment>
            <Head>
                <title>My Travel Records</title>
            </Head>
            <Row>
                <Col xs={12} lg={6}>
                    {records.length > 0
                    	? 
	                    	<Table striped bordered hover>
		                        <thead>
		                            <tr>
		                                <th>Origin</th>
		                                <th>Destination</th>
		                                <th>Date</th>
		                                <th>Distance (m)</th>
		                                <th>Duration (mins)</th>
                                        <th>Amount (PHP)</th>
		                            </tr>
		                        </thead>
		                        <tbody>
		                            {records.map(record => {
		                                return (
		                                    <tr key={record._id}>
		                                        <td onClick={() => setCoordinates(record.origin.longitude, record.origin.latitude)}>
                                    	    		{record.origin.longitude}, {record.origin.latitude}
                                    	    	</td>
                                            	<td onClick={() => setCoordinates(record.destination.longitude, record.destination.latitude)}>
                                            		{record.destination.longitude}, {record.destination.latitude}
                                            	</td>
		                                        <td>{moment(record.date).format('MMMM DD YYYY')}</td>
		                                        <td>{Math.round(record.distance)}</td>
		                                        <td>{Math.round(record.duration/60)}</td>
                                                <td>{record.charge ? record.charge.amount : 'N/A'}</td>
		                                    </tr>
		                                )
		                            })}
		                        </tbody>
		                    </Table>
                    	: 
                    		<Alert variant="info">You have no travel records yet.</Alert>
                    }
                </Col>
            	<Col xs={12} lg={6}>
                    <div className="mapContainer" ref={mapContainerRef} />
                </Col>
            </Row>
        </React.Fragment>
	)
}