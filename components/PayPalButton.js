import React from 'react';
import ReactDOM from 'react-dom';
const PayPalBtn = paypal.Buttons.driver("react", { React, ReactDOM });

export default function PayPalButton({ amount, completeBooking }) {

	const createOrder = (data, actions) => {
		return actions.order.create({
			purchase_units: [
				{
					amount: {
						value: amount
					}
				}
			]
		})
	}

	const onApprove = (data, actions) => {

		console.log(data);

		// Calls the completeBooking function to reset the travel details and record the booking in our database
		completeBooking(data.orderID);

		// Capture this transaction in your PayPal account dashboard
		return actions.order.capture()

	}

	return(
		<PayPalBtn 
			createOrder={(data, actions) => createOrder(data, actions)}
			onApprove={(data, actions) => onApprove(data, actions)}
		/>
	)

}

/*

data {
	orderId: "sdfaefwef45we5f"
}

actions {
	create: () => {},
	capture: () => {}
}

*/

// function completeBooking(params){

// }

// completeBooking(orderId)