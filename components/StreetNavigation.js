import { useState, useEffect, useRef, useContext } from 'react';
import Router from 'next/router';
import { Row, Col, Card, Button, Alert } from 'react-bootstrap';
import mapboxgl from 'mapbox-gl';
mapboxgl.accessToken = process.env.NEXT_PUBLIC_REACT_APP_MAPBOX_KEY;
import MapboxDirections from '@mapbox/mapbox-gl-directions/dist/mapbox-gl-directions';
import UserContext from '../UserContext';
import PayPalButton from './PayPalButton';

const StreetNavigation = () => {

	const { user } = useContext(UserContext);

	// Component states for relevant route information
	const [distance, setDistance] = useState(0);
	const [duration, setDuration] = useState(0);
	const [originLong, setOriginLong] = useState(0);
	const [originLat, setOriginLat] = useState(0);
	const [destinationLong, setDestinationLong] = useState(0);
	const [destinationLat, setDestinationLat] = useState(0);
	// State for determining submit button availability
	const [isActive, setIsActive] = useState(false);
	// Will store the amount/price for the booking/order
	const [amount, setAmount] = useState(0);

	const mapContainerRef = useRef(null);

	// Function for sending the route information to API for saving in database
    function recordTravel(orderId) {

        fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/users/travels`, {
            method: 'POST',
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('token')}`,
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                originLong: originLong,
                originLat: originLat,
                destinationLong: destinationLong,
                destinationLat: destinationLat,
                duration: duration,
                distance: distance,
                orderId: orderId,
                amount: amount
            })
        })
        .then(res => res.json())
        .then(data => {

            if(data === true){
                Router.push('/history')
            }else{
                Router.push('/error')
            }

        })

    }

    function completeBooking(orderId) {
    	console.log(orderId);

    	setOriginLong(0);
    	setOriginLat(0);
    	setDestinationLong(0);
    	setDestinationLat(0);
    	setDistance(0);
    	setDuration(0);
    	setAmount(0);
    	recordTravel(orderId);
    }

	useEffect(() =>{

		// Instantiates a new Mapbox Map object
		const map = new mapboxgl.Map({
			// Sets the container for the map
			container: mapContainerRef.current,
			style: 'mapbox://styles/mapbox/streets-v11',
			center: [121.04382, 14.63289],
			zoom: 12
		})

		// Instantiates mapbox directions control overlay
		const directions = new MapboxDirections({
			accessToken: mapboxgl.accessToken,
			unit: 'metric',
			profile: 'mapbox/driving'
		})

		// Add navigation control (+/- zoom buttons)
		map.addControl(new mapboxgl.NavigationControl(), 'bottom-right' );

		// Add Mapbox Directions control overlay to the map
		map.addControl(directions, 'top-left');

		// Whenever a route is generated it returns an array of route objects
		directions.on("route", e => {

			console.log(e);

			// Capture the relevant route information in their respective component states 
			setOriginLong(e.route[0].legs[0].steps[0].intersections[0].location[0])
			setOriginLat(e.route[0].legs[0].steps[0].intersections[0].location[1])
			setDestinationLong(e.route[0].legs[0].steps[e.route[0].legs[0].steps.length-1].intersections[0].location[0])
			setDestinationLat(e.route[0].legs[0].steps[e.route[0].legs[0].steps.length-1].intersections[0].location[1])
			setDistance(e.route[0].distance)
			setDuration(e.route[0].duration)
			// Add the computation for the booking/order
			setAmount(Math.round(e.route[0].distance/1000)*50)

		})

		// Clean up and release all resources/maps associated when the component unmounts
		// Not doing so, will result in increasing memory usage that will crash your application
		return () => map.remove()

	}, [])

	// Toggles the state of the submit button when the route information is generated.
    useEffect(() => {
        if(distance !== 0 && duration !== 0){
            setIsActive(true)
        }else{
            setIsActive(false)
        }
    }, [distance, duration])

	return(
		<Row>
			<Col xs={12} md={8}>
				<div className="mapContainer" ref={mapContainerRef}/>
			</Col>
			<Col xs={12} md={4}>
				{ user.id === null
					?
						<Alert variant="info">
							You must be logged in to record your travels.
						</Alert>
					:
						<Card>
							<Card.Body>
								<Card.Title>
									Record Route
								</Card.Title>
								<Card.Text>
									Origin Longitude: {originLong}
								</Card.Text>
								<Card.Text>
									Origin Latitude: {originLat}
								</Card.Text>
								<Card.Text>
									Desination Longitude: {destinationLong}
								</Card.Text>
								<Card.Text>
									Desination Latitude: {destinationLat}
								</Card.Text>
								<Card.Text>
									Total Distance: {Math.round(distance)} meters
								</Card.Text>
								<Card.Text>
									Total Duration: {Math.round(duration/60)} minutes
								</Card.Text>
								<Card.Text>
									Booking Cost: PHP {amount}
								</Card.Text>
								{ isActive === true
									?
										<PayPalButton 
											amount={amount}
											completeBooking={completeBooking}
										/>
									:
										<Alert variant="info">
											Generate a rounte to book a ride
										</Alert>
								}
							</Card.Body>
						</Card>
				}
			</Col>
		</Row>
	)
}

export default StreetNavigation;